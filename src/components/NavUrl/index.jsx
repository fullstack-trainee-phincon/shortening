import PropTypes from 'prop-types';
import { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import LightModeIcon from '@mui/icons-material/LightMode';
import NightsStayIcon from '@mui/icons-material/NightsStay';

import { setLocale, setTheme } from '@containers/App/actions';

import FlagId from '@static/images/flags/id.png';
import FlagEn from '@static/images/flags/en.png';

import classes from './style.module.scss';

const NavUrl = ({ title, locale, contentRef, theme }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [menuPosition, setMenuPosition] = useState(null);
  const open = Boolean(menuPosition);
  const [opens, setOpens] = useState(false);

  const handleClick = (event) => {
    setMenuPosition(event.currentTarget);
  };

  const handleClose = () => {
    setMenuPosition(null);
  };

  const handleTheme = () => {
    dispatch(setTheme(theme === 'light' ? 'dark' : 'light'));
  };

  const onSelectLang = (lang) => {
    if (lang !== locale) {
      dispatch(setLocale(lang));
    }
    handleClose();
  };

  // const goHome = () => {
  //   navigate('/');
  // };

  return (
    <AppBar className={classes.headerWrapper} ref={contentRef}>
      <div className={classes.contentWrapper}>
        <div className={classes.LogoMenuWrapper}>
          <div className={classes.Logo}>Shortly</div>
          <div className={classes.Menu}>
            <div>Features</div>
            <div>Pricing</div>
            <div>Resources</div>
          </div>
        </div>

        <div className={classes.LoginWrapper}>
          <div className={classes.LoginBox}>
            <div>Login</div>
          </div>
          <div className={classes.SignUpBox}>
            <div>SignUp</div>
          </div>
        </div>
        <div className={classes.toolbar}>
          <div className={classes.theme} onClick={handleTheme}>
            {theme === 'light' ? <NightsStayIcon /> : <LightModeIcon />}
          </div>
          <div className={classes.toggle} onClick={handleClick}>
            <Avatar className={classes.avatar} src={locale === 'id' ? FlagId : FlagEn} />
            <div className={classes.lang}>{locale}</div>
            <ExpandMoreIcon />
          </div>
        </div>
        <Menu open={open} anchorEl={menuPosition} onClose={handleClose}>
          <MenuItem onClick={() => onSelectLang('id')} selected={locale === 'id'}>
            <div className={classes.menu}>
              <Avatar className={classes.menuAvatar} src={FlagId} />
              <div className={classes.menuLang}>
                <FormattedMessage id="app_lang_id" />
              </div>
            </div>
          </MenuItem>
          <MenuItem onClick={() => onSelectLang('en')} selected={locale === 'en'}>
            <div className={classes.menu}>
              <Avatar className={classes.menuAvatar} src={FlagEn} />
              <div className={classes.menuLang}>
                <FormattedMessage id="app_lang_en" />
              </div>
            </div>
          </MenuItem>
        </Menu>
      </div>
    </AppBar>
  );
};

NavUrl.propTypes = {
  title: PropTypes.string,
  locale: PropTypes.string.isRequired,
  theme: PropTypes.string,
  contentRef: PropTypes.object,
};

export default NavUrl;
