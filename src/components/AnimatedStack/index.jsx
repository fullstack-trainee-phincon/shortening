// import React from 'react';
// import animationData from '../../static/images/Lottie Lego.json';

// const LottieAnimation = () => {
//   return <Lottie animationData={animationData} loop={true} autoplay={true} />;
// };

// export default LottieAnimation;

import React from 'react';
import Lottie from 'lottie-react';
import classes from './style.module.scss';
import animationData from '../../static/images/social-animation.json';

const LottieAnimation = () => {
  return (
    <>
      <Lottie className={classes.SocialAnimation} animationData={animationData} loop={true} autoplay={true} />
    </>
  );
};

export default LottieAnimation;
