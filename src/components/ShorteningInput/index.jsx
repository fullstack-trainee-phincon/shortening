import React, { useState, useRef } from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import classes from '@components/ShorteningInput/style.module.scss';
import Button from '@mui/material/Button';
import { useDispatch, useSelector } from 'react-redux';
import { getShortUrl } from '@containers/App/actions';
import InputAdornment from '@mui/material/InputAdornment';

export default function ShorteningInput() {
  const shortUrl = useSelector((state) => state.app.shortUrl);
  const [shortUrlError, setShortUrlError] = React.useState(null);
  const [cooldown, setCooldown] = useState(false);
  const dispatch = useDispatch();
  // HANDLE INPUT
  const [inputValue, setInputValue] = useState('');

  // const inputRef = useRef(null);

  const handleSubmit = () => {
    dispatch(getShortUrl(inputValue));
  };

  const handleChange = (event) => {
    setInputValue(event.target.value);
  };

  // Handles for remove url Dialog
  const [open, setOpen] = useState(false);
  const [selectedCode, setSelectedCode] = useState('');

  const handleClickOpen = (code) => {
    setOpen(true);
    setSelectedCode(code);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteShortUrl(selectedCode));
    setSelectedCode('');

    setOpen(false);
  };

  const onKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
    } else {
      setCooldown(true);
      setTimeout(() => {
        setCooldown(false);
      }, 2000);
    }
    return dispatch(getShortUrl(inputValue));
  };

  // Contents
  const renderContent = () => (
    <div className={classes.row}>
      <div id="input_empty" />
    </div>
  );

  return (
    <div className={classes.ShorteningInput}>
      <div className={classes.ShorteningBox}>
        <Box
          component="form"
          sx={{
            '& > :not(style)': { m: 1, width: '60ch' },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            inputProps={{
              style: { backgroundColor: 'var(--color-input-props) ', fontSize: '16px' },
              onKeyPress,
            }}
            id="input"
            label="Paste your link"
            variant="outlined"
            onChange={handleChange}
          />

          {shortUrlError && <div className={classes.error}>{handleError()}</div>}
        </Box>

        <Button className={classes.ShorteningButton} type="submit" onClick={handleSubmit} disabled={cooldown}>
          Shorten It!
        </Button>
      </div>

      {shortUrl.length > 0
        ? shortUrl?.map((url) => (
            <Box key={url.code}>
              <TextField
                inputProps={{ style: { backgroundColor: 'var(--color-input-props) ' } }}
                id="input"
                label=""
                variant="outlined"
                value={url.short_link}
              />
              <Button
                variant="contained"
                style={{ color: 'white', backgroundColor: 'var(--color-button-cyan)' }}
                onClick={() => {
                  navigator.clipboard.writeText(url.short_link);
                }}
              >
                Copy Link
              </Button>
            </Box>
          ))
        : renderContent()}
    </div>
  );
}
