export default {
  app_greeting: 'Hi from Web!',
  app_title_header: 'Vite + React',
  app_not_found: 'Page not found',
  app_lang_id: 'Indonesian',
  app_lang_en: 'English',
  app_MainTitle1: 'More than',
  app_MainTitle2: 'just shorter link',
};
