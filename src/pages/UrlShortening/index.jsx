import React from 'react';
import Section1 from '@pages/UrlShortening/Section1/index.jsx';
import Section2 from '@pages/UrlShortening/Section2/index.jsx';
import Section3 from '@pages/UrlShortening/Section3/index.jsx';

const Url = () => {
  return (
    <>
      <Section1 />
      <Section2 />
      <Section3 />
    </>
  );
};

export default Url;
