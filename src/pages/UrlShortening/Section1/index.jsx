import React from 'react';
import classes from './style.module.scss';
import { FormattedMessage } from 'react-intl';
import bgPicture from '@static/images/illustration-working.svg';
import ShorteningInput from '@components/ShorteningInput/index';

const Section1 = () => {
  return (
    <div>
      <div className={classes.UrlWrapper}>
        <div className={classes.ContainerLeft}>
          <div className={classes.LeftWrapper}>
            <div className={classes.TextMore}>
              {/* More than just <br /> shorter link */}
              <FormattedMessage id="app_MainTitle1" /> <br />
              <FormattedMessage id="app_MainTitle2" />
            </div>
            <div className={classes.SubTitle}>
              Build your brand's recognition and get detailed <br /> insight on how your links are performing
            </div>
            <button className={classes.GetStarted}>Get Started</button>
          </div>
        </div>
        <div className={classes.ContainerRight}>
          <img src={bgPicture} />
        </div>
        <div className={classes.ShorteningWrapper}>
          <ShorteningInput />
        </div>
      </div>
    </div>
  );
};

export default Section1;
